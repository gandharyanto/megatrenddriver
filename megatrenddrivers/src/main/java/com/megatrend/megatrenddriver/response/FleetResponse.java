package com.megatrend.megatrenddriver.response;

import com.megatrend.megatrenddriver.object.FleetObject;

import java.util.List;

public class FleetResponse {

    List<FleetObject> msg;

    public List<FleetObject> getMsg() {
        return msg;
    }

    public void setMsg(List<FleetObject> msg) {
        this.msg = msg;
    }
}
