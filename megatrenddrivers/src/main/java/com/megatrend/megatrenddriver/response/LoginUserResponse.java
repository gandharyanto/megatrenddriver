package com.megatrend.megatrenddriver.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 3/3/2018.
 */

public class LoginUserResponse {
    @SerializedName("idSalesman")
    String idSalesman;
    @SerializedName("idBranch")
    String idBranch;
    @SerializedName("employee_id")
    String employee_id;

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getIdSalesman() {
        return idSalesman;
    }

    public void setIdSalesman(String idSalesman) {
        this.idSalesman = idSalesman;
    }

    public String getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(String idBranch) {
        this.idBranch = idBranch;
    }
}
