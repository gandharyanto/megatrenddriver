package com.megatrend.megatrenddriver.response;

import com.megatrend.megatrenddriver.object.FleetDetailObject;

import java.util.List;

public class FleetDetailResponse {
    List<FleetDetailObject> msg;

    public List<FleetDetailObject> getMsg() {
        return msg;
    }

    public void setMsg(List<FleetDetailObject> msg) {
        this.msg = msg;
    }
}
