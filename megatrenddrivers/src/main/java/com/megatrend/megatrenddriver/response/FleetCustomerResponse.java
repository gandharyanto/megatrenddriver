package com.megatrend.megatrenddriver.response;

import com.megatrend.megatrenddriver.object.FleetCustomerObject;

import java.util.List;

public class FleetCustomerResponse {
    List<FleetCustomerObject> msg;

    public List<FleetCustomerObject> getMsg() {
        return msg;
    }

    public void setMsg(List<FleetCustomerObject> msg) {
        this.msg = msg;
    }
}
