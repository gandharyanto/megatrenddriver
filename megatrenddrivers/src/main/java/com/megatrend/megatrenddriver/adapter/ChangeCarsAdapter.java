package com.megatrend.megatrenddriver.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.megatrend.megatrenddriver.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeCarsAdapter extends RecyclerView.Adapter<ChangeCarsAdapter.MyViewHolder> {

    Context context;
    Activity activity;
    List<String> cars;

    public ChangeCarsAdapter(Context context, Activity activity, List<String> cars) {
        this.context = context;
        this.activity = activity;
        this.cars = cars;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_cars, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvCarType.setText(cars.get(position));
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivCars)
        ImageView ivCars;
        @BindView(R.id.tvCarType)
        TextView tvCarType;
        @BindView(R.id.tvNo)
        TextView tvNo;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
