package com.megatrend.megatrenddriver.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.activity.DetailCustomerActivity;
import com.megatrend.megatrenddriver.object.FleetDetailObject;
import com.megatrend.megatrenddriver.utils.PreferenceManagers;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitiesLogAdapter extends RecyclerView.Adapter<ActivitiesLogAdapter.MyViewHolder> {

    Context mContext;
    List<FleetDetailObject> fleetDetailResponse;

    public ActivitiesLogAdapter(Context mContext, List<FleetDetailObject> fleetDetailResponse) {
        this.mContext = mContext;
        this.fleetDetailResponse = fleetDetailResponse;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_activities, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final FleetDetailObject fleetDetailObject = fleetDetailResponse.get(position);
        holder.tvNo.setText(String.valueOf(position+1));
        holder.tvNama.setText(fleetDetailObject.getCustname());
        holder.tvBox.setText(fleetDetailObject.getQty());
        holder.tvArea.setText(fleetDetailObject.getArea());
        holder.tvStatus.setText(fleetDetailObject.getStatus());
        holder.clAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fleetDetailObject.getStatus().equals("Done") || fleetDetailObject.getStatus().equals("Cancel") || fleetDetailObject.getStatus().equals("Failed")) {

                } else {
                    String trip = PreferenceManagers.getData("fleet_trip_id", mContext);
                    if (trip != null) {
                        Intent intent = new Intent(mContext, DetailCustomerActivity.class);
                        intent.putExtra("customer_id", fleetDetailObject.getCustomer_id());
                        intent.putExtra("customer_name", fleetDetailObject.getCustname());
                        mContext.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return fleetDetailResponse.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNo)
        TextView tvNo;
        @BindView(R.id.tvNama)
        TextView tvNama;
        @BindView(R.id.tvBox)
        TextView tvBox;
        @BindView(R.id.tvArea)
        TextView tvArea;
        @BindView(R.id.tvStatus)
        TextView tvStatus;
        @BindView(R.id.clAct)
        ConstraintLayout clAct;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
