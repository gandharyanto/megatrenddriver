package com.megatrend.megatrenddriver.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.object.FleetCustomerObject;
import com.megatrend.megatrenddriver.response.FleetCustomerResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DOCustomerAdapter  extends RecyclerView.Adapter<DOCustomerAdapter.MyViewHolder>{

    Context mContext;
    List<FleetCustomerObject> fleetCustomerResponse;

    public DOCustomerAdapter(Context mContext, List<FleetCustomerObject> fleetCustomerResponse) {
        this.mContext = mContext;
        this.fleetCustomerResponse = fleetCustomerResponse;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_customer, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final FleetCustomerObject fleetCustomerObject = fleetCustomerResponse.get(position);
        holder.tvNo.setText(String.valueOf(position+1));
        holder.tvDO.setText(fleetCustomerObject.getDelivery_order_no());
        holder.tvScan.setText(fleetCustomerObject.getScanned());
        holder.tvSN.setText(fleetCustomerObject.getSerial_no());

    }

    @Override
    public int getItemCount() {
        return fleetCustomerResponse.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNo)
        TextView tvNo;
        @BindView(R.id.tvDO)
        TextView tvDO;
        @BindView(R.id.tvSN)
        TextView tvSN;
        @BindView(R.id.tvScan)
        TextView tvScan;
        @BindView(R.id.clAct)
        ConstraintLayout clAct;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
