package com.megatrend.megatrenddriver.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.megatrend.megatrenddriver.DatabaseHelper;
import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.object.DetailCustomerObject;
import com.megatrend.megatrenddriver.utils.PreferenceManagers;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListDOCustomerAdapter extends RecyclerView.Adapter<ListDOCustomerAdapter.MyViewHolder> {
    Context mContext;
    List<DetailCustomerObject> detailCustomerObjects;
    DatabaseHelper databaseHelper;

    public ListDOCustomerAdapter(Context mContext, List<DetailCustomerObject> detailCustomerObjects) {
        this.mContext = mContext;
        this.detailCustomerObjects = detailCustomerObjects;
        databaseHelper = new DatabaseHelper(mContext);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_customer, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DetailCustomerObject detailCustomerObject = detailCustomerObjects.get(position);
        holder.tvNo.setText(String.valueOf(position + 1));
        holder.tvDO.setText(detailCustomerObject.getDelivery_order_no());
        if (detailCustomerObject.getScanned().equals(detailCustomerObject.getBox())) {
            holder.tvScan.setText("Yes");
        } else {
            holder.tvScan.setText("No");
        }
        holder.tvSN.setText(detailCustomerObject.getScanned() + "/" + detailCustomerObject.getBox());
        holder.clAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serialDone = databaseHelper.selectedSerialDone(detailCustomerObject.getFleet_customer_id(),detailCustomerObject.getDelivery_order_no() );
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(serialDone)
                        .setCancelable(false)
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog,
                                                        int id) {
                                        dialog.dismiss();
                                    }
                                }).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return detailCustomerObjects.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNo)
        TextView tvNo;
        @BindView(R.id.tvDO)
        TextView tvDO;
        @BindView(R.id.tvSN)
        TextView tvSN;
        @BindView(R.id.tvScan)
        TextView tvScan;
        @BindView(R.id.clAct)
        ConstraintLayout clAct;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
