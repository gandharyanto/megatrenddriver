package com.megatrend.megatrenddriver.object;

public class DetailCustomerObject {
    String fleet_trip_id, fleet_customer_id, delivery_order_no, box, scanned;

    public String getFleet_trip_id() {
        return fleet_trip_id;
    }

    public void setFleet_trip_id(String fleet_trip_id) {
        this.fleet_trip_id = fleet_trip_id;
    }

    public String getFleet_customer_id() {
        return fleet_customer_id;
    }

    public void setFleet_customer_id(String fleet_customer_id) {
        this.fleet_customer_id = fleet_customer_id;
    }

    public String getDelivery_order_no() {
        return delivery_order_no;
    }

    public void setDelivery_order_no(String delivery_order_no) {
        this.delivery_order_no = delivery_order_no;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }

    public String getScanned() {
        return scanned;
    }

    public void setScanned(String scanned) {
        this.scanned = scanned;
    }
}
