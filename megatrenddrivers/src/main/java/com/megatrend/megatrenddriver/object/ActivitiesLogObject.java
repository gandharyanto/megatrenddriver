package com.megatrend.megatrenddriver.object;

public class ActivitiesLogObject {
    String no, nama, total_box, area, status;

    public ActivitiesLogObject(String no, String nama, String total_box, String area, String status) {
        this.no = no;
        this.nama = nama;
        this.total_box = total_box;
        this.area = area;
        this.status = status;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTotal_box() {
        return total_box;
    }

    public void setTotal_box(String total_box) {
        this.total_box = total_box;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
