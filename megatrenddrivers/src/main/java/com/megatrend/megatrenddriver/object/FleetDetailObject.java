package com.megatrend.megatrenddriver.object;

public class FleetDetailObject {
    String fleet_trip_id, custname, customer_id, qty, area, status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFleet_trip_id() {
        return fleet_trip_id;
    }

    public void setFleet_trip_id(String fleet_trip_id) {
        this.fleet_trip_id = fleet_trip_id;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
