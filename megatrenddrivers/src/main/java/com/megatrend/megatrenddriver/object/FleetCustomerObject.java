package com.megatrend.megatrenddriver.object;

public class FleetCustomerObject {
    String number, fleet_trip_id, custname, area, delivery_order_no, serial_no, scanned;

    public String getScanned() {
        return scanned;
    }

    public void setScanned(String scanned) {
        this.scanned = scanned;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFleet_trip_id() {
        return fleet_trip_id;
    }

    public void setFleet_trip_id(String fleet_trip_id) {
        this.fleet_trip_id = fleet_trip_id;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDelivery_order_no() {
        return delivery_order_no;
    }

    public void setDelivery_order_no(String delivery_order_no) {
        this.delivery_order_no = delivery_order_no;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }
}
