package com.megatrend.megatrenddriver.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.megatrend.megatrenddriver.DatabaseHelper;
import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.utils.PreferenceManagers;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.edtMerek)
    EditText edtMerek;
    @BindView(R.id.edtType)
    EditText edtType;
    @BindView(R.id.edtNomor)
    EditText edtNomor;
    @BindView(R.id.cvEdit)
    CardView cvEdit;
    @BindView(R.id.ivFoto)
    ImageView ivFoto;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBarcode)
    ImageView ivBarcode;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.vToolbar)
    AppBarLayout vToolbar;

    Context mContext;
    Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        mContext = this;
        mActivity = this;
        tvTitle.setText("User Profile");
        final DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
        ivBarcode.setVisibility(View.INVISIBLE);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManagers.clear(mActivity);
                databaseHelper.dropAll();

                Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
    }
}
