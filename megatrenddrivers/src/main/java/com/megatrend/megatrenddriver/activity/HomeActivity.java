package com.megatrend.megatrenddriver.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.megatrend.megatrenddriver.DatabaseHelper;
import com.megatrend.megatrenddriver.MyInterface;
import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.adapter.ActivitiesLogAdapter;
import com.megatrend.megatrenddriver.api.ApiUtils;
import com.megatrend.megatrenddriver.component.ProgressDialogManager;
import com.megatrend.megatrenddriver.object.FleetDetailObject;
import com.megatrend.megatrenddriver.response.FleetDetailResponse;
import com.megatrend.megatrenddriver.response.FleetResponse;
import com.megatrend.megatrenddriver.response.StatusResponse;
import com.megatrend.megatrenddriver.utils.BarcodeScanUtils;
import com.megatrend.megatrenddriver.utils.PreferenceManagers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends FragmentActivity implements OnMapReadyCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBarcode)
    ImageView ivBarcode;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.vToolbar)
    AppBarLayout vToolbar;
    @BindView(R.id.cvMaps)
    CardView cvMaps;
    @BindView(R.id.tvJenis)
    TextView tvJenis;
    @BindView(R.id.cvCar)
    LinearLayout cvCar;
    @BindView(R.id.rvActivity)
    RecyclerView rvActivity;
    @BindView(R.id.tvlbl)
    TextView tvlbl;
    @BindView(R.id.tvNo)
    TextView tvNo;
    @BindView(R.id.tvNama)
    TextView tvNama;
    @BindView(R.id.tvBox)
    TextView tvBox;
    @BindView(R.id.tvArea)
    TextView tvArea;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.clToolbar)
    ConstraintLayout clToolbar;
    @BindView(R.id.ivHome)
    ImageView ivHome;
    @BindView(R.id.ivRecent)
    ImageView ivRecent;
    @BindView(R.id.ivProfile)
    ImageView ivProfile;
    @BindView(R.id.nav)
    ConstraintLayout nav;
    @BindView(R.id.tvStart)
    TextView tvStart;
    @BindView(R.id.cvStart)
    CardView cvStart;
    @BindView(R.id.ivMaps)
    ImageView ivMaps;
    private GoogleMap mMap;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    FusedLocationProviderClient mFusedLocationClient;
    Context mContext;
    Activity mActivity;
    BarcodeScanUtils.CheckPermission barcodeScanUtils;
    private static final int REQUEST_SCANNER_BARCODE = 49374;
    DatabaseHelper databaseHelper;
    String employee_id;
    ProgressDialogManager progressDialogManager;
    boolean started;
    boolean isStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        mContext = this;
        mActivity = this;
        databaseHelper = new DatabaseHelper(mContext);
        progressDialogManager = new ProgressDialogManager(this);

        ivBack.setVisibility(View.INVISIBLE);
        barcodeScanUtils = new BarcodeScanUtils.CheckPermission();
        employee_id = PreferenceManagers.getData("employee_id", mContext);

        tvTitle.setText("Megatrend");
        started = false;
        if (PreferenceManagers.hasData("started", mContext)) {
            started = Boolean.valueOf(PreferenceManagers.getData("started", mContext));
            if (started) {
                tvStart.setText("Finish");
            } else {
                tvStart.setText("Start");
            }
        }
        ivMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr=" + "20.344,34.34" + "&daddr=" + "20.5666,45.345"));
                        Uri.parse("https://www.google.com/maps/search/?api=1"));
                startActivity(intent);
            }
        });
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        cvMaps.setVisibility(View.GONE);

        /*cvChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//ini buat intent ke halaman ganti mobil
                Intent intent = new Intent(HomeActivity.this, ChangeCarsActivity.class);
                startActivity(intent);
            }
        });*/
        DatabaseHelper.exportDB();

        ivRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//ini buat intent ke halaman history
                Intent intent = new Intent(HomeActivity.this, RecentActivity.class);
                startActivity(intent);

            }
        });

        ivBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//ini buat intent ke halaman scan barcode
                barcodeScanUtils.CheckPermissions(mActivity);

            }
        });

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//ini buat inten ke halaman user profile
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        tvStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Anda yakin ? ")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog,
                                                        int id) {
//                                    progressDialogManager.show();
                                        String trip = PreferenceManagers.getData("fleet_trip_id", mContext);
                                        if (trip != null) {
                                            if (PreferenceManagers.hasData("started", mContext)) {
                                                started = Boolean.valueOf(PreferenceManagers.getData("started", mContext));
                                                if (!started) {
                                                    tvStart.setText("Finish");
                                                    PreferenceManagers.setDataWithSameKey("started", "true", mContext);
                                                    String date = getDateCurrentTimeZone();
                                                    Toast.makeText(mContext, date, Toast.LENGTH_LONG).show();
                                                    startStopFleet();
                                                } else {
                                                    tvStart.setText("Start");
                                                    PreferenceManagers.setDataWithSameKey("started", "false", mContext);
                                                    String date = getDateCurrentTimeZone();
                                                    Toast.makeText(mContext, date, Toast.LENGTH_LONG).show();
                                                    startStopFleet();
                                                }
                                            } else {
                                                tvStart.setText("Finish");
                                                PreferenceManagers.setDataWithSameKey("started", "true", mContext);
                                                String date = getDateCurrentTimeZone();
                                                Toast.makeText(mContext, date, Toast.LENGTH_LONG).show();
                                                startStopFleet();
                                            }
                                        }
                                    }
                                })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

//        setupActivities();

        getFleetData();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        /*if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        mMap.setMyLocationEnabled(true);
        // Check if we were successful in obtaining the map.
        if (mMap != null) {


            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                                                   @Override
                                                   public void onMyLocationChange(Location location) {
                                                       //mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("It's Me!"));
                                                       mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()),500));
                                                   }
                                               }
            );
        }*/

        /*// Add a marker in Sydney and move the camera
        LatLng grahapool = new LatLng(-6.588540, 106.787939);//buat nentuin lokasinya
        mMap.addMarker(new MarkerOptions().position(grahapool).title("Marker in GRAHA POOL"));//buat ngasih
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(grahapool,500));*/

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000); // two minute interval
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mMap.setMyLocationEnabled(true);
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        Intent intent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr=" + "20.344,34.34" + "&daddr=" + "20.5666,45.345"));
                                Uri.parse("https://www.google.com/maps/search/?api=1"));
                        startActivity(intent);
                    }
                });
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mMap.setMyLocationEnabled(true);
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                //Place current location marker
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Current Position");
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                //mCurrLocationMarker = mMap.addMarker(markerOptions);

                //move map camera
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
            }
        }
    };

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(HomeActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SCANNER_BARCODE: {
                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                if (result != null) {
                    if (result.getContents() == null) {
                        //dialogMessage.displayMessage(mActivity, "result.getContents() == null", "Scan canceled");
                    } else {
                        /*Intent intent = new Intent(mActivity, ProductSearchActivity.class);
                        intent.putExtra("code", result.getContents());
                        startActivity(intent);*/
                        Toast.makeText(mContext, result.getContents(), Toast.LENGTH_LONG);
                    }
                } else {
                    super.onActivityResult(requestCode, resultCode, data);
                }
                break;
            }
        }
    }

    private void getFleetData() {
        progressDialogManager.show();
        started = Boolean.valueOf(PreferenceManagers.getData("started", mContext));
        if (!started) {
            ApiUtils.GetFleet(employee_id, new MyInterface.GetFleetCallback() {
                @Override
                public void onSuccess(@NonNull FleetResponse value) {
                    if (value.getMsg().size() != 0) {
                        tvJenis.setText(value.getMsg().get(0).getNopol());
                        PreferenceManagers.setDataWithSameKey("fleet_trip_id", value.getMsg().get(0).getId(), mContext);
                        getFleetDetailData();
                    } else {
                        if (progressDialogManager.isShowing())
                            progressDialogManager.dismiss();
                    }
                }

                @Override
                public void onFailed(@NonNull String value) {
                    if (progressDialogManager.isShowing())
                        progressDialogManager.dismiss();
                }

                @Override
                public void onError(@NonNull Throwable throwable) {
                    if (progressDialogManager.isShowing())
                        progressDialogManager.dismiss();
                }
            });
        } else {
            final List<FleetDetailObject> fleetDetailResponseList = databaseHelper.selectedFD();
            ActivitiesLogAdapter activitiesLogAdapter = new ActivitiesLogAdapter(mContext, fleetDetailResponseList);
            rvActivity.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
            rvActivity.setAdapter(activitiesLogAdapter);
            if (progressDialogManager.isShowing())
                progressDialogManager.dismiss();
        }
    }

    private void getFleetDetailData() {
        ApiUtils.GetFleetDetail(employee_id, new MyInterface.GetFleetDetailCallback() {
            @Override
            public void onSuccess(@NonNull FleetDetailResponse value) {
                final FleetDetailResponse fleetDetailResponseList = value;

                for (int i = 0; i < fleetDetailResponseList.getMsg().size(); i++) {
                    String cust = databaseHelper.selectedCust(fleetDetailResponseList.getMsg().get(i).getCustomer_id());
                    if (!cust.equals(fleetDetailResponseList.getMsg().get(i).getCustomer_id())) {
                        databaseHelper.insertFleetDetail(fleetDetailResponseList.getMsg().get(i).getFleet_trip_id(),
                                fleetDetailResponseList.getMsg().get(i).getCustname(),
                                fleetDetailResponseList.getMsg().get(i).getCustomer_id(),
                                fleetDetailResponseList.getMsg().get(i).getArea(),
                                fleetDetailResponseList.getMsg().get(i).getQty(),
                                "OnProgress");
                    }
                }

                final List<FleetDetailObject> fleetDetailObjects = databaseHelper.selectedFD();
                ActivitiesLogAdapter activitiesLogAdapter = new ActivitiesLogAdapter(mContext, fleetDetailObjects);
                rvActivity.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
                rvActivity.setAdapter(activitiesLogAdapter);
                if (progressDialogManager.isShowing())
                    progressDialogManager.dismiss();
            }

            @Override
            public void onFailed(@NonNull String value) {
                if (progressDialogManager.isShowing())
                    progressDialogManager.dismiss();
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                if (progressDialogManager.isShowing())
                    progressDialogManager.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFleetData();
//        final List<FleetDetailObject> fleetDetailObjects = databaseHelper.selectedFD();
//        ActivitiesLogAdapter activitiesLogAdapter = new ActivitiesLogAdapter(mContext, fleetDetailObjects);
//        rvActivity.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
//        rvActivity.setAdapter(activitiesLogAdapter);
    }

    public String getDateCurrentTimeZone() {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currenTimeZone = calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "";
    }

    public void startStopFleet() {
        started = Boolean.valueOf(PreferenceManagers.getData("started", mContext));
        final Map<String, String> data = new HashMap<>();
        if (started) {
            data.put("start_time", getDateCurrentTimeZone());
            ApiUtils.FleetStart(PreferenceManagers.getData("fleet_trip_id", mContext), data, new MyInterface.FleetStartStopCallback() {
                @Override
                public void onSuccess(@NonNull StatusResponse value) {
                    Toast.makeText(mContext, value.getMsg(), Toast.LENGTH_LONG);
                }

                @Override
                public void onFailed(@NonNull String value) {

                }

                @Override
                public void onError(@NonNull Throwable throwable) {

                }
            });
            /*AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage("Anda yakin ? ")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog,
                                                    int id) {
//                                    progressDialogManager.show();
                                    ApiUtils.FleetStart(PreferenceManagers.getData("fleet_trip_id", mContext), data, new MyInterface.FleetStartStopCallback() {
                                        @Override
                                        public void onSuccess(@NonNull StatusResponse value) {
                                            Toast.makeText(mContext, value.getMsg(), Toast.LENGTH_LONG);
                                        }

                                        @Override
                                        public void onFailed(@NonNull String value) {

                                        }

                                        @Override
                                        public void onError(@NonNull Throwable throwable) {

                                        }
                                    });
                                }
                            })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int id) {
                            dialog.dismiss();
                        }
                    }).show();*/
        } else {
            data.put("end_time", getDateCurrentTimeZone());
            ApiUtils.FleetEnd(PreferenceManagers.getData("fleet_trip_id", mContext), data, new MyInterface.FleetStartStopCallback() {
                @Override
                public void onSuccess(@NonNull StatusResponse value) {
                    databaseHelper.dropAll();
                    Toast.makeText(mContext, value.getMsg(), Toast.LENGTH_LONG);
                    getFleetData();
                }

                @Override
                public void onFailed(@NonNull String value) {

                }

                @Override
                public void onError(@NonNull Throwable throwable) {

                }
            });
            /*AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage("Anda yakin ? ")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(final DialogInterface dialog,
                                                    int id) {
//                                    progressDialogManager.show();
                                    ApiUtils.FleetEnd(PreferenceManagers.getData("fleet_trip_id", mContext), data, new MyInterface.FleetStartStopCallback() {
                                        @Override
                                        public void onSuccess(@NonNull StatusResponse value) {
                                            Toast.makeText(mContext, value.getMsg(), Toast.LENGTH_LONG);
                                        }

                                        @Override
                                        public void onFailed(@NonNull String value) {

                                        }

                                        @Override
                                        public void onError(@NonNull Throwable throwable) {

                                        }
                                    });
                                }
                            })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int id) {
                            dialog.dismiss();
                        }
                    }).show();*/
        }
    }
}
