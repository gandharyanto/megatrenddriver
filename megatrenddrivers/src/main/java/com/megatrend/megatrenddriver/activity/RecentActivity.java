package com.megatrend.megatrenddriver.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.adapter.ChangeCarsAdapter;
import com.megatrend.megatrenddriver.adapter.RecentAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentActivity extends AppCompatActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBarcode)
    ImageView ivBarcode;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.vToolbar)
    AppBarLayout vToolbar;
    @BindView(R.id.rv)
    RecyclerView rv;
    private Context mContext;
    private Activity mActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_recent);
        ButterKnife.bind(this);

        mContext = this;
        mActivity = this;

        tvTitle.setText("History Log");
        ivBarcode.setVisibility(View.INVISIBLE);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        List<String> recentList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            recentList.add("Inv " + (i + 1));
        }
        RecentAdapter changeCarsAdapter = new RecentAdapter(mContext, mActivity, recentList);
        rv.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
        rv.setAdapter(changeCarsAdapter);
    }
}
