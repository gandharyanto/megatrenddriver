package com.megatrend.megatrenddriver.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.megatrend.megatrenddriver.DatabaseHelper;
import com.megatrend.megatrenddriver.MyInterface;
import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.adapter.ListDOCustomerAdapter;
import com.megatrend.megatrenddriver.api.ApiUtils;
import com.megatrend.megatrenddriver.component.ProgressDialogManager;
import com.megatrend.megatrenddriver.object.DetailCustomerObject;
import com.megatrend.megatrenddriver.object.FleetCustomerObject;
import com.megatrend.megatrenddriver.response.FleetCustomerResponse;
import com.megatrend.megatrenddriver.response.StatusResponse;
import com.megatrend.megatrenddriver.utils.BarcodeScanUtils;
import com.megatrend.megatrenddriver.utils.PreferenceManagers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailCustomerActivity extends AppCompatActivity {

    String customer_id, customer_name;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBarcode)
    ImageView ivBarcode;
    @BindView(R.id.clToolbar)
    ConstraintLayout clToolbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.vToolbar)
    AppBarLayout vToolbar;
    @BindView(R.id.llLabel)
    LinearLayout llLabel;
    @BindView(R.id.cvBarcode)
    CardView cvBarcode;
    @BindView(R.id.rvActivity)
    RecyclerView rvActivity;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    Context mContext;
    Activity mActivity;
    ProgressDialogManager progressDialogManager;
    DatabaseHelper databaseHelper;
    BarcodeScanUtils.CheckPermission barcodeScanUtils;
    private static final int REQUEST_SCANNER_BARCODE = 49374;
    ListDOCustomerAdapter listDOCustomerAdapter;
    List<FleetCustomerObject> fleetCustomerObjects;
    @BindView(R.id.edtInputCode)
    EditText edtInputCode;
    @BindView(R.id.cvScan)
    CardView cvScan;
    Boolean started;
    @BindView(R.id.tvCustName)
    TextView tvCustName;
    @BindView(R.id.tvNo)
    TextView tvNo;
    @BindView(R.id.tvDO)
    TextView tvDO;
    @BindView(R.id.tvSN)
    TextView tvSN;
    @BindView(R.id.tvScan)
    TextView tvScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_customer);
        ButterKnife.bind(this);

        tvTitle.setText("Detail Customer");
        mContext = this;
        mActivity = this;
        customer_id = "";
        customer_name = "";
        tvTitle.setText("Detail Customer");
        progressDialogManager = new ProgressDialogManager(mContext);
        barcodeScanUtils = new BarcodeScanUtils.CheckPermission();
        databaseHelper = new DatabaseHelper(mContext);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            customer_id = intent.getStringExtra("customer_id");
            customer_name = intent.getStringExtra("customer_name");
        }
        tvSN.setText("Scan/Total");
        tvScan.setText("Sukses");

        tvCustName.setText(customer_name);
        ivBarcode.setVisibility(View.INVISIBLE);
        progressDialogManager.show();

        /*cvBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//ini buat intent ke halaman scan barcode
                barcodeScanUtils.CheckPermissions(mActivity);
            }
        });*/

        edtInputCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {

                if (id == EditorInfo.IME_ACTION_NEXT || id == EditorInfo.IME_NULL) {
                    if (started) {
                        String scanned = databaseHelper.selectedScanned(edtInputCode.getText().toString(), customer_id);
                        if (scanned.equals("No")) {
                            databaseHelper.updateFleetCustomer(edtInputCode.getText().toString(), "Yes");
                            Toast.makeText(mContext, edtInputCode.getText().toString() + " Yes", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(mContext, edtInputCode.getText().toString() + " Salah", Toast.LENGTH_LONG).show();
                        }

//                        fleetCustomerObjects = databaseHelper.selectedFC(customer_id);

                        List<DetailCustomerObject> detailCustomerObjects = databaseHelper.selectScannedSerial(customer_id);
                        listDOCustomerAdapter = new ListDOCustomerAdapter(mContext, detailCustomerObjects);
                        rvActivity.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
                        rvActivity.setAdapter(listDOCustomerAdapter);
                        edtInputCode.setText("");
                    } else {
                        Toast.makeText(mContext, "Anda belum memulai perjalanan", Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
                return false;
            }
        });

        cvScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (started) {
                    barcodeScanUtils.CheckPermissions(mActivity);
                } else {
                    Toast.makeText(mContext, "Anda belum memulai perjalanan", Toast.LENGTH_LONG).show();

                }
            }
        });

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

//        fleetCustomerObjects = databaseHelper.selectedFC(customer_id);

        List<DetailCustomerObject> detailCustomerObjects = databaseHelper.selectScannedSerial(customer_id);
        listDOCustomerAdapter = new ListDOCustomerAdapter(mContext, detailCustomerObjects);
        rvActivity.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
        rvActivity.setAdapter(listDOCustomerAdapter);
        started = Boolean.valueOf(PreferenceManagers.getData("started", mContext));

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String done = databaseHelper.selectedCustomer(customer_id);
                if (!done.equals("No")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Anda yakin ? ")
                            .setCancelable(false)
                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog,
                                                            int id) {
//                                    progressDialogManager.show();
                                            if (started) {
                                                Map<String, String> data = new HashMap<>();
                                                data.put("customer_id", customer_id);
                                                ApiUtils.DoDone(PreferenceManagers.getData("fleet_trip_id", mContext), data, new MyInterface.FleetStartStopCallback() {
                                                    @Override
                                                    public void onSuccess(@NonNull StatusResponse value) {
                                                        databaseHelper.updateFleetDetail(customer_id, "Done");
                                                        Toast.makeText(mContext, value.getMsg(), Toast.LENGTH_LONG).show();
                                                    }

                                                    @Override
                                                    public void onFailed(@NonNull String value) {
                                                        Toast.makeText(mContext, value, Toast.LENGTH_LONG).show();
                                                    }

                                                    @Override
                                                    public void onError(@NonNull Throwable throwable) {
                                                        Toast.makeText(mContext, throwable.getMessage(), Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                            } else {
                                                Toast.makeText(mContext, "Anda belum memulai perjalanan", Toast.LENGTH_LONG).show();

                                            }
                                        }
                                    })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.dismiss();
                                }
                            }).show();
                } else {
                    //dialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Ada yang belum di scan tetap submit ? ")
                            .setCancelable(false)
                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog,
                                                            int id) {
                                            databaseHelper.updateFleetDetail(customer_id, "Failed");
                                            dialog.dismiss();
                                            finish();
                                        }
                                    })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Anda yakin ? ")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(final DialogInterface dialog,
                                                        int id) {
//                                    progressDialogManager.show();
                                        if (started) {
                                            databaseHelper.dropDO();
                                            databaseHelper.updateFleetDetail(customer_id, "Cancel");
                                            finish();
                                        } else {
                                            Toast.makeText(mContext, "Anda belum memulai perjalanan", Toast.LENGTH_LONG).show();

                                        }
                                    }
                                })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

        ApiUtils.GetFleetCustomer(customer_id, new MyInterface.GetFleetCustomerCallback() {
            @Override
            public void onSuccess(@NonNull FleetCustomerResponse value) {
                final FleetCustomerResponse fleetCustomerResponse = value;

                String delivery_order = databaseHelper.selectedDO(fleetCustomerResponse.getMsg().get(0).getDelivery_order_no());
                if (delivery_order.equals("")) {
                    for (int i = 0; i < fleetCustomerResponse.getMsg().size(); i++) {
                        databaseHelper.insertFleetCustomer(fleetCustomerResponse.getMsg().get(i).getFleet_trip_id(),
                                fleetCustomerResponse.getMsg().get(i).getCustname(),
                                customer_id,
                                fleetCustomerResponse.getMsg().get(i).getArea(),
                                fleetCustomerResponse.getMsg().get(i).getDelivery_order_no(),
                                fleetCustomerResponse.getMsg().get(i).getSerial_no(),
                                "No");
                    }
                }

//                fleetCustomerObjects = databaseHelper.selectedFC(customer_id);

                List<DetailCustomerObject> detailCustomerObjects = databaseHelper.selectScannedSerial(customer_id);
                listDOCustomerAdapter = new ListDOCustomerAdapter(mContext, detailCustomerObjects);
                rvActivity.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
                rvActivity.setAdapter(listDOCustomerAdapter);
                if (progressDialogManager.isShowing())
                    progressDialogManager.dismiss();
            }

            @Override
            public void onFailed(@NonNull String value) {

                if (progressDialogManager.isShowing())
                    progressDialogManager.dismiss();
            }

            @Override
            public void onError(@NonNull Throwable throwable) {

                if (progressDialogManager.isShowing())
                    progressDialogManager.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SCANNER_BARCODE: {
                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                if (result != null) {
                    if (result.getContents() == null) {
                        //dialogMessage.displayMessage(mActivity, "result.getContents() == null", "Scan canceled");
                        Toast.makeText(mContext, "result.getContents() == null", Toast.LENGTH_LONG).show();
                    } else {
                        String scanned = databaseHelper.selectedScanned(result.getContents(), customer_id);
                        if (scanned.equals("No")) {
                            databaseHelper.updateFleetCustomer(result.getContents(), "Yes");
                            Toast.makeText(mContext, result.getContents() + " Yes", Toast.LENGTH_LONG).show();
                        } else if (scanned.equals("Wrong")) {
                            Toast.makeText(mContext, result.getContents() + " Salah", Toast.LENGTH_LONG).show();
                        }

//                        fleetCustomerObjects = databaseHelper.selectedFC(customer_id);

                        List<DetailCustomerObject> detailCustomerObjects = databaseHelper.selectScannedSerial(customer_id);
                        listDOCustomerAdapter = new ListDOCustomerAdapter(mContext, detailCustomerObjects);
                        rvActivity.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
                        rvActivity.setAdapter(listDOCustomerAdapter);
                    }
                } else {
                    super.onActivityResult(requestCode, resultCode, data);
                }
                break;
            }
        }
    }
}
