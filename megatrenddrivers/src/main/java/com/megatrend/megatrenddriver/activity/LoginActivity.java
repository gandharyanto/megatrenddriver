package com.megatrend.megatrenddriver.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.megatrend.megatrenddriver.MyInterface;
import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.api.ApiUtils;
import com.megatrend.megatrenddriver.component.ProgressDialogManager;
import com.megatrend.megatrenddriver.json.JsonLogin;
import com.megatrend.megatrenddriver.response.LoginUserResponse;
import com.megatrend.megatrenddriver.utils.PermissionsManager;
import com.megatrend.megatrenddriver.utils.PreferenceManagers;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.tusername)
    EditText tusername;
    @BindView(R.id.cvEmail)
    CardView cvEmail;
    @BindView(R.id.tpassword)
    EditText tpassword;
    @BindView(R.id.cvPass)
    CardView cvPass;
    @BindView(R.id.cvLogin)
    CardView cvLogin;
    ProgressDialogManager progressDialogManager;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progressDialogManager = new ProgressDialogManager(this);
        mContext = this;
//        tusername.setText("bram.permana@megatrend.co.id");
//        tpassword.setText("admin");

        if (PreferenceManagers.hasData("employee_id", mContext)) {
            Intent i = new Intent(LoginActivity.this, HomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }

        tusername.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT) {
                    tpassword.requestFocus();
                    return true;
                }
                return false;
            }
        });

        tpassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    cvLogin.performClick();
                    return true;
                }
                return false;
            }
        });

        cvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialogManager.show();

                //if (ApiUtils.isURLReachable(mContext, "http://google.com")) {
                JsonLogin jsonLogin = new JsonLogin(tusername.getText().toString(), tpassword.getText().toString());
                ApiUtils.LoginUser(jsonLogin, new MyInterface.LoginUserCallback() {
                    @Override
                    public void onSuccess(@NonNull LoginUserResponse value) {
                        if (progressDialogManager.isShowing())
                            progressDialogManager.dismiss();
                        PreferenceManagers.setDataWithSameKey("employee_id", value.getEmployee_id(), mContext);
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailed(@NonNull String value) {
                        if (progressDialogManager.isShowing())
                            progressDialogManager.dismiss();
                    }

                    @Override
                    public void onError(@NonNull Throwable throwable) {
                        if (progressDialogManager.isShowing())
                            progressDialogManager.dismiss();
                    }
                });
            }
        });

        if (!PermissionsManager.hasPermissions(this, PermissionsManager.PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PermissionsManager.PERMISSIONS, PermissionsManager.PERMISSION_ALL);
        }
    }
}
