package com.megatrend.megatrenddriver.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.megatrend.megatrenddriver.R;
import com.megatrend.megatrenddriver.adapter.ChangeCarsAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeCarsActivity extends AppCompatActivity {

    Context mContext;
    Activity mActivity;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBarcode)
    ImageView ivBarcode;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv)
    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_cars);
        ButterKnife.bind(this);

        mContext = this;
        mActivity = this;

        tvTitle.setText("Ganti Mobil");
        ivBarcode.setVisibility(View.INVISIBLE);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        List<String> merekDataObjectList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            merekDataObjectList.add("Car " + (i + 1));
        }
        ChangeCarsAdapter changeCarsAdapter = new ChangeCarsAdapter(mContext, mActivity, merekDataObjectList);
        rv.setLayoutManager(new GridLayoutManager(mContext, 1, GridLayoutManager.VERTICAL, false));
        rv.setAdapter(changeCarsAdapter);
    }
}
