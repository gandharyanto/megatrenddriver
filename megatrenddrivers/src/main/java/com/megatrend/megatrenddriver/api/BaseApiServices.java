package com.megatrend.megatrenddriver.api;

import com.megatrend.megatrenddriver.json.JsonLogin;
import com.megatrend.megatrenddriver.response.FleetCustomerResponse;
import com.megatrend.megatrenddriver.response.FleetDetailResponse;
import com.megatrend.megatrenddriver.response.FleetResponse;
import com.megatrend.megatrenddriver.response.LoginUserResponse;
import com.megatrend.megatrenddriver.response.StatusResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface BaseApiServices {

    @GET("/backend/public/api/android/getFleet/{id_driver}")
    Call<FleetResponse> getFleet(@Path("id_driver") String id_driver);

    @GET("/backend/public/api/android/getFleetDetail/{id_driver}")
    Call<FleetDetailResponse> getFleetDetail(@Path("id_driver") String id_driver);

    @GET("/backend/public/api/android/getFleetCustomer/{customer_id}")
    Call<FleetCustomerResponse> getFleetCustomer(@Path("customer_id") String customer_id);

    @POST("/backend/public/api/android/fleetStart/{fleet_trip_id}")
    Call<StatusResponse> fleetStart(@Path("fleet_trip_id") String fleet_trip_id,
                                    @QueryMap Map<String, String> options);

    @POST("/backend/public/api/android/fleetEnd/{fleet_trip_id}")
    Call<StatusResponse> fleetEnd(@Path("fleet_trip_id") String fleet_trip_id,
                                  @QueryMap Map<String, String> options);

    @POST("/backend/public/api/android/userlogin")
    Call<LoginUserResponse> loginUser(@Body JsonLogin jsonData);

    @POST("/backend/public/api/android/dodone/{fleet_trip_id}")
    Call<StatusResponse> doDone(@Path("fleet_trip_id") String fleet_trip_id,
                                @QueryMap Map<String, String> options);
}
