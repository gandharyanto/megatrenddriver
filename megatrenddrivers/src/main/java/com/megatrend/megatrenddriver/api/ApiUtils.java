package com.megatrend.megatrenddriver.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.megatrend.megatrenddriver.MyInterface;
import com.megatrend.megatrenddriver.json.JsonLogin;
import com.megatrend.megatrenddriver.response.FleetCustomerResponse;
import com.megatrend.megatrenddriver.response.FleetDetailResponse;
import com.megatrend.megatrenddriver.response.FleetResponse;
import com.megatrend.megatrenddriver.response.LoginUserResponse;
import com.megatrend.megatrenddriver.response.StatusResponse;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiUtils {

    public static final String BASE_URL_API1 = "http://megatrend.co.id/";
    private static final String TAG = "ApiUtils";
    private static BaseApiServices mApiService;

    public static BaseApiServices getAPIService() {
        return RetrofitClient.getClient(BASE_URL_API1).create(BaseApiServices.class);
    }

    public static boolean isURLReachable(Context context, String uRL) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.
                        CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            try {
                URL url = new URL(uRL);   // Change to "http://google.com" for www test.

                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setConnectTimeout(5 * 1000);          // 5 s.

                urlc.connect();
                if (urlc.getResponseCode() == 200) { // 200 = "OK" code  (http connection is fine).
                    Log.i(TAG, "Success !");
                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                return false;
            }
        }
        return false;
    }

    public static void GetFleet(String idDriver, final MyInterface.GetFleetCallback mCallback) {
        mApiService = getAPIService();
        mApiService.getFleet(idDriver).enqueue(new Callback<FleetResponse>() {
            @Override
            public void onResponse(Call<FleetResponse> call, Response<FleetResponse> response) {
                if (response.code() == 200) {
                    mCallback.onSuccess(response.body());
                } else {
                    mCallback.onFailed(response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<FleetResponse> call, Throwable t) {
                mCallback.onError(t);
            }
        });
    }

    public static void GetFleetDetail(String idDriver, final MyInterface.GetFleetDetailCallback mCallback) {
        mApiService = getAPIService();
        mApiService.getFleetDetail(idDriver).enqueue(new Callback<FleetDetailResponse>() {
            @Override
            public void onResponse(Call<FleetDetailResponse> call, Response<FleetDetailResponse> response) {
                if (response.code() == 200) {
                    mCallback.onSuccess(response.body());
                } else {
                    mCallback.onFailed(response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<FleetDetailResponse> call, Throwable t) {
                mCallback.onError(t);
            }
        });
    }

    public static void GetFleetCustomer(String idCustomer, final MyInterface.GetFleetCustomerCallback mCallback) {
        mApiService = getAPIService();
        mApiService.getFleetCustomer(idCustomer).enqueue(new Callback<FleetCustomerResponse>() {
            @Override
            public void onResponse(Call<FleetCustomerResponse> call, Response<FleetCustomerResponse> response) {
                if (response.code() == 200) {
                    mCallback.onSuccess(response.body());
                } else {
                    mCallback.onFailed(response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<FleetCustomerResponse> call, Throwable t) {
                mCallback.onError(t);
            }
        });
    }

    public static void FleetStart(String fleet_trip_id, Map<String,String> data, final MyInterface.FleetStartStopCallback mCallback) {
        mApiService = getAPIService();
        mApiService.fleetStart(fleet_trip_id, data).enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                if (response.code() == 200) {
                    mCallback.onSuccess(response.body());
                } else {
                    mCallback.onFailed(response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                mCallback.onError(t);
            }
        });
    }

    public static void FleetEnd(String fleet_trip_id, Map<String,String> data, final MyInterface.FleetStartStopCallback mCallback) {
        mApiService = getAPIService();
        mApiService.fleetEnd(fleet_trip_id, data).enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                if (response.code() == 200) {
                    mCallback.onSuccess(response.body());
                } else {
                    mCallback.onFailed(response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                mCallback.onError(t);
            }
        });
    }

    public static void DoDone(String fleet_trip_id, Map<String,String> data, final MyInterface.FleetStartStopCallback mCallback) {
        mApiService = getAPIService();
        mApiService.doDone(fleet_trip_id, data).enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                if (response.code() == 200) {
                    mCallback.onSuccess(response.body());
                } else {
                    mCallback.onFailed(response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                mCallback.onError(t);
            }
        });
    }

    public static void LoginUser(JsonLogin jsonPost, final MyInterface.LoginUserCallback mCallback) {
        mApiService = getAPIService();
        mApiService.loginUser(jsonPost).enqueue(new Callback<LoginUserResponse>() {
            @Override
            public void onResponse(Call<LoginUserResponse> call, Response<LoginUserResponse> response) {
                if (response.code() == 200) {
                    mCallback.onSuccess(response.body());
                } else {
                    mCallback.onFailed(response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<LoginUserResponse> call, Throwable t) {
                mCallback.onError(t);
            }
        });
    }
}