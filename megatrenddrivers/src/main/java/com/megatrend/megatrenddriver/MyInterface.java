package com.megatrend.megatrenddriver;

import android.support.annotation.NonNull;

import com.megatrend.megatrenddriver.response.FleetCustomerResponse;
import com.megatrend.megatrenddriver.response.FleetDetailResponse;
import com.megatrend.megatrenddriver.response.FleetResponse;
import com.megatrend.megatrenddriver.response.LoginUserResponse;
import com.megatrend.megatrenddriver.response.StatusResponse;

public class MyInterface {

    public interface GetFleetCallback{
        void onSuccess(@NonNull FleetResponse value);

        void onFailed(@NonNull String value);

        void onError(@NonNull Throwable throwable);
    }

    public interface GetFleetDetailCallback{
        void onSuccess(@NonNull FleetDetailResponse value);

        void onFailed(@NonNull String value);

        void onError(@NonNull Throwable throwable);
    }

    public interface GetFleetCustomerCallback{
        void onSuccess(@NonNull FleetCustomerResponse value);

        void onFailed(@NonNull String value);

        void onError(@NonNull Throwable throwable);
    }

    public interface FleetStartStopCallback{
        void onSuccess(@NonNull StatusResponse value);

        void onFailed(@NonNull String value);

        void onError(@NonNull Throwable throwable);
    }

    public interface LoginUserCallback{
        void onSuccess(@NonNull LoginUserResponse value);

        void onFailed(@NonNull String value);

        void onError(@NonNull Throwable throwable);
    }
}
