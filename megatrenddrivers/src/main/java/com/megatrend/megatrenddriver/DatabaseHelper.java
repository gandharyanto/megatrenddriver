package com.megatrend.megatrenddriver;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import com.megatrend.megatrenddriver.object.DetailCustomerObject;
import com.megatrend.megatrenddriver.object.FleetCustomerObject;
import com.megatrend.megatrenddriver.object.FleetDetailObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    private static final String DB_NAME = "DRIVER.DB";
    private static final String FILE_NAME = "DRIVER";
    private static final int DB_VERSION = 2;
    private static SQLiteDatabase mDatabase = null;
    Context mContext;

    private static final String FLEET_TABLE = "fleet";
    private static final String F_ID = "_id";
    private static final String F_API_ID = "id";
    private static final String F_NOPOL = "nopol";
    private static final String F_FIRST_NAME = "first_name";
    private static final String F_DATE = "date";
    private static final String F_START_TIME = "start_time";
    private static final String F_END_TIME = "end_time";

    private static final String CREATE_FLEET_TABLE = "CREATE TABLE '"
            + FLEET_TABLE + "' ("
            + "'" + F_ID + "' INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "'" + F_API_ID + "' TEXT, "
            + "'" + F_NOPOL + "' TEXT, "
            + "'" + F_FIRST_NAME + "' TEXT, "
            + "'" + F_DATE + "' TEXT, "
            + "'" + F_START_TIME + "' TEXT, "
            + "'" + F_END_TIME + "' TEXT "
            + ")";

    private static final String FLEET_DETAIL_TABLE = "fleet_detail";
    private static final String FD_ID = "_id";
    private static final String FD_FLEET_TRIP_ID = "fleet_trip_id";
    private static final String FD_CUSTNAME = "custname";
    private static final String FD_CUSTOMER_ID = "customer_id";
    private static final String FD_QTY = "qty";
    private static final String FD_AREA = "area";
    private static final String FD_STATUS = "status";

    private static final String CREATE_FLEET_DETAIL_TABLE = "CREATE TABLE '"
            + FLEET_DETAIL_TABLE + "' ("
            + "'" + FD_ID + "' INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "'" + FD_FLEET_TRIP_ID + "' TEXT, "
            + "'" + FD_CUSTNAME + "' TEXT, "
            + "'" + FD_CUSTOMER_ID + "' TEXT, "
            + "'" + FD_QTY + "' TEXT, "
            + "'" + FD_AREA + "' TEXT, "
            + "'" + FD_STATUS + "' TEXT "
            + ")";

    private static final String FLEET_CUSTOMER_TABLE = "fleet_customer";
    private static final String FC_ID = "_id";
    private static final String FC_TRIP_ID = "fleet_trip_id";
    private static final String FC_CUSTOMER_ID = "fleet_customer_id";
    private static final String FC_CUSTNAME = "custname";
    private static final String FC_AREA = "area";
    private static final String FC_DELIVERY_ORDER_NO = "delivery_order_no";
    private static final String FC_SERIAL_NO = "serial_no";
    private static final String FC_SCAN = "scan";

    private static final String CREATE_FLEET_CUSTOMER_TABLE = "CREATE TABLE '"
            + FLEET_CUSTOMER_TABLE + "' ("
            + "'" + FC_ID + "' INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "'" + FC_TRIP_ID + "' TEXT, "
            + "'" + FC_CUSTOMER_ID + "' TEXT, "
            + "'" + FC_CUSTNAME + "' TEXT, "
            + "'" + FC_AREA + "' TEXT, "
            + "'" + FC_DELIVERY_ORDER_NO + "' TEXT, "
            + "'" + FC_SERIAL_NO + "' TEXT, "
            + "'" + FC_SCAN + "' TEXT "
            + ")";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;
        Log.i(TAG, CREATE_FLEET_TABLE);
        Log.i(TAG, CREATE_FLEET_DETAIL_TABLE);
        Log.i(TAG, CREATE_FLEET_CUSTOMER_TABLE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FLEET_TABLE);
        db.execSQL(CREATE_FLEET_DETAIL_TABLE);
        db.execSQL(CREATE_FLEET_CUSTOMER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FLEET_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + FLEET_DETAIL_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + FLEET_CUSTOMER_TABLE);
        onCreate(db);
    }

    public void dropAll() {
        mDatabase = this.getWritableDatabase();
        mDatabase.execSQL("DROP TABLE IF EXISTS " + FLEET_CUSTOMER_TABLE);
        mDatabase.execSQL("DROP TABLE IF EXISTS " + FLEET_TABLE);
        mDatabase.execSQL("DROP TABLE IF EXISTS " + FLEET_DETAIL_TABLE);
        mDatabase.execSQL(CREATE_FLEET_TABLE);
        mDatabase.execSQL(CREATE_FLEET_DETAIL_TABLE);
        mDatabase.execSQL(CREATE_FLEET_CUSTOMER_TABLE);

    }

    public void dropDO() {
        mDatabase = this.getWritableDatabase();
        mDatabase.execSQL("DROP TABLE IF EXISTS " + FLEET_CUSTOMER_TABLE);
        mDatabase.execSQL(CREATE_FLEET_CUSTOMER_TABLE);

    }

    public void insertFleetDetail(String fleet_trip_id, String custname, String cust_id, String area, String qty, String status) {
        mDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FD_FLEET_TRIP_ID, fleet_trip_id);
        contentValues.put(FD_CUSTNAME, custname);
        contentValues.put(FD_CUSTOMER_ID, cust_id);
        contentValues.put(FD_AREA, area);
        contentValues.put(FD_QTY, qty);
        contentValues.put(FD_STATUS, status);
        long id = mDatabase.insertWithOnConflict(FLEET_DETAIL_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        Log.i(TAG, id + " (" + custname + ", " + status + ")");
    }

    public void updateFleetDetail(String cust_id, String status) {
        mDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FD_STATUS, status);
        mDatabase.update(FLEET_DETAIL_TABLE, contentValues, FD_CUSTOMER_ID + " = '" + cust_id + "'", null);
        Log.i(TAG, " (" + cust_id + ", " + status + ")");
    }

    public List<FleetDetailObject> selectedFD() {
        List<FleetDetailObject> fleetDetailObjects = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + FLEET_DETAIL_TABLE /*+
                " WHERE " + FD + " = '" + customer_id + "'"*/;

        Log.i(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                FleetDetailObject fleetDetailObject = new FleetDetailObject();
                fleetDetailObject.setCustomer_id(c.getString(c.getColumnIndex(FD_CUSTOMER_ID)));
                fleetDetailObject.setCustname(c.getString(c.getColumnIndex(FD_CUSTNAME)));
                fleetDetailObject.setFleet_trip_id(c.getString(c.getColumnIndex(FD_FLEET_TRIP_ID)));
                fleetDetailObject.setArea(c.getString(c.getColumnIndex(FD_AREA)));
                fleetDetailObject.setQty(c.getString(c.getColumnIndex(FD_QTY)));
                fleetDetailObject.setStatus(c.getString(c.getColumnIndex(FD_STATUS)));
                fleetDetailObjects.add(fleetDetailObject);
            } while (c.moveToNext());
        }

        c.close();

        return fleetDetailObjects;
    }

    public String selectedCust(String customer_id) {
        String cust_id = "";

        String selectQuery = "SELECT * FROM " + FLEET_DETAIL_TABLE +
                " WHERE " + FD_CUSTOMER_ID + " = '" + customer_id + "'";

        Log.i(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                cust_id = c.getString(c.getColumnIndex(FD_CUSTOMER_ID));
            } while (c.moveToNext());
        }

        c.close();

        return cust_id;
    }

    public void insertFleetCustomer(String trip_id, String custname, String cust_id, String area, String delivery_order_no, String sn, String scan) {
        mDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FC_TRIP_ID, trip_id);
        contentValues.put(FC_CUSTNAME, custname);
        contentValues.put(FC_CUSTOMER_ID, cust_id);
        contentValues.put(FC_AREA, area);
        contentValues.put(FC_DELIVERY_ORDER_NO, delivery_order_no);
        contentValues.put(FC_SERIAL_NO, sn);
        contentValues.put(FC_SCAN, scan);
        long id = mDatabase.insertWithOnConflict(FLEET_CUSTOMER_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        Log.i(TAG, id + " (" + custname + ", " + delivery_order_no + ")");
    }

    public void updateFleetCustomer(String sn, String scan) {
        mDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FC_SCAN, scan);
        mDatabase.update(FLEET_CUSTOMER_TABLE, contentValues, FC_SERIAL_NO + " = '" + sn + "'", null);
        Log.i(TAG, " (" + sn + ", " + scan + ")");
    }


    public List<FleetCustomerObject> selectedFC(String customer_id) {
        List<FleetCustomerObject> fleetCustomerObjects = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + FLEET_CUSTOMER_TABLE +
                " WHERE " + FC_CUSTOMER_ID + " = '" + customer_id + "'";

        Log.i(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                FleetCustomerObject fleetCustomerObject = new FleetCustomerObject();
                fleetCustomerObject.setNumber(c.getString(c.getColumnIndex(FC_ID)));
                fleetCustomerObject.setCustname(c.getString(c.getColumnIndex(FC_CUSTNAME)));
                fleetCustomerObject.setFleet_trip_id(c.getString(c.getColumnIndex(FC_TRIP_ID)));
                fleetCustomerObject.setArea(c.getString(c.getColumnIndex(FC_AREA)));
                fleetCustomerObject.setDelivery_order_no(c.getString(c.getColumnIndex(FC_DELIVERY_ORDER_NO)));
                fleetCustomerObject.setSerial_no(c.getString(c.getColumnIndex(FC_SERIAL_NO)));
                fleetCustomerObject.setScanned(c.getString(c.getColumnIndex(FC_SCAN)));
                fleetCustomerObjects.add(fleetCustomerObject);
            } while (c.moveToNext());
        }

        c.close();

        return fleetCustomerObjects;
    }


    public String selectedScanned(String sn, String cust_id) {
        String scanned = "Wrong";

        String selectQuery = "SELECT * FROM " + FLEET_CUSTOMER_TABLE +
                " WHERE " + FC_SERIAL_NO + " = '" + sn + "' AND " + FC_CUSTOMER_ID + " = '" + cust_id + "'";

        Log.i(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                scanned = c.getString(c.getColumnIndex(FC_SCAN));
            } while (c.moveToNext());
        }

        c.close();

        return scanned;
    }

    public String selectedCustomer(String cust_id) {
        String scanned = "Ok";

        String selectQuery = "SELECT * FROM " + FLEET_CUSTOMER_TABLE +
                " WHERE " + FC_SCAN + " = 'No' AND " + FC_CUSTOMER_ID + " = '" + cust_id + "'";

        Log.i(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                scanned = c.getString(c.getColumnIndex(FC_SCAN));
            } while (c.moveToNext());
        }

        c.close();

        return scanned;
    }

    public List<DetailCustomerObject> selectScannedSerial(String cust_id) {
        List<DetailCustomerObject> scanned = new ArrayList<>();

        String selectQuery = "select fleet_trip_id, fleet_customer_id, delivery_order_no, count(delivery_order_no) as box, ifnull(scanned, 0)as scanned" +
                " from fleet_customer left join (select delivery_order_no as do, count(delivery_order_no) as scanned from fleet_customer where fleet_customer_id = '" +
                cust_id + "' and scan = 'Yes' group by delivery_order_no) as temp1 on fleet_customer.delivery_order_no = do where fleet_customer_id = '" +
                cust_id + "' group by delivery_order_no";

        Log.i(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                DetailCustomerObject detailCustomerObject = new DetailCustomerObject();
                detailCustomerObject.setFleet_customer_id(c.getString(c.getColumnIndex("fleet_customer_id")));
                detailCustomerObject.setDelivery_order_no(c.getString(c.getColumnIndex("delivery_order_no")));
                detailCustomerObject.setBox(c.getString(c.getColumnIndex("box")));
                detailCustomerObject.setScanned(c.getString(c.getColumnIndex("scanned")));
                scanned.add(detailCustomerObject);
            } while (c.moveToNext());
        }

        c.close();

        return scanned;
    }

    public String selectedSerialDone(String cust_id, String doNum) {
        String serial = "";

        String selectQuery = "select serial_no from fleet_customer where fleet_customer_id = '" +
                cust_id + "' and scan = 'Yes' and delivery_order_no = '" + doNum + "'";

        Log.i(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                serial += c.getString(c.getColumnIndex(FC_SERIAL_NO)) + ", ";
            } while (c.moveToNext());
        }

        c.close();

        return serial;
    }

    public String selectedDO(String sn) {
        String delivery_order = "";

        String selectQuery = "SELECT * FROM " + FLEET_CUSTOMER_TABLE +
                " WHERE " + FC_DELIVERY_ORDER_NO + " = '" + sn + "'";

        Log.i(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                delivery_order = c.getString(c.getColumnIndex(FC_DELIVERY_ORDER_NO));
            } while (c.moveToNext());
        }

        c.close();

        return delivery_order;
    }

    public static void exportDB() {
        // TODO Auto-generated method stub
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + "com.megatrend.megatrenddriver"
                        + "//databases//" + DB_NAME;
                String backupDBPath = "/Download/" + DB_NAME;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                Log.e("", "Sukses : " + backupDBPath);
            }
        } catch (Exception e) {
            //Toast.makeText(act, e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("", "gagal export (" + e.getMessage() + ")");
        }
    }
}
